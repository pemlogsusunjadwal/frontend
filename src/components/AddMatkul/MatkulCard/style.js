import styled from "styled-components";

export const Container = styled.div`
    width: 100px;
    background-color: red;
    margin: 5px;
    padding: 5px;
    display: flex;
    flex-direction:column;
    color: white;
    border-radius:5px;
`;