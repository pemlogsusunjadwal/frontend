import constant from "../constant";

function getHost() {
  const { host, protocol } = window.location;
  return encodeURI(`${protocol}//${host}`);
}

export default {
  SSOLogin() {
    const SSOWindow = window.open(
      `${constant.API_BASE_URL}api/account/auth/?sender=${getHost()}`,
      "SSO UI",
      "left=50, top=50, width=480, height=480"
    );

    // TODO: Promise should be resolved when the window is closed.
    return new Promise(function(resolve) {
      window.addEventListener(
        "message",
        e => {
          if (`${e.origin}/` === constant.API_BASE_URL) {
            if (SSOWindow) SSOWindow.close();
            resolve(e.data.body);
          } else {
            const error = new Error("SSO Login failed, please try again.");
            // logError(error, e.data)
            console.log(error, e.data);
          }
        },
        { once: true }
      );
    });
  }
};
