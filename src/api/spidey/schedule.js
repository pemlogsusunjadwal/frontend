import axios from "axios";
import constant from "../constant";

export default {
  getCourses: (kdOrg, period) =>
    axios.get(`${constant.API_BASE_URL}api/schedule/courses/${period}/${kdOrg}/`)
};
