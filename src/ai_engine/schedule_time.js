export const ScheduleTimeRule =
    "jam(Day,H_Start,M_Start,H_End,_, Res) :- H_Start < H_End,Res =.. [Day,H_Start,M_Start]." +
    "jam(Day, H_Start,M_Start,H_Start,M_End,Res) :- M_Start =< M_End, Res =.. [Day,H_Start,M_Start]." +
    "jam(Day, H_Start,M_Start,H_Start,M_End, Res) :- M_Start < M_End,!, M_Start < 59, M_Temp is M_Start+1, jam(Day, H_Start,M_Temp,H_Start,M_End,Res)." +
    "jam(Day, H_Start,M_Start,H_End,M_End, Res) :- H_Start < H_End, M_Start < 59,!, M_Temp is M_Start+1, jam(Day,H_Start,M_Temp,H_End,M_End,Res)." +
    "jam(Day, H_Start,_,H_End,M_End, Res) :- H_Start < H_End, H_Temp is H_Start+1, jam(Day,H_Temp,0,H_End,M_End,Res).";

export const CourseCombinationRule =
    "comb2(_,[])." +
    "comb2([X|T],[X|Comb]):-comb2(T,Comb)." +
    "comb2([_|T],[X|Comb]):-comb2(T,[X|Comb])."+
    "countSks([],Max,Max)."+
    "countSks([H|T],Before,Max) :- sks(H,Val), NewTotal is Before + Val, NewTotal =< Max, countSks(T,NewTotal,Max)."+
    "getMatkulCombination(X,CombList,MaxSks) :- comb2(X,CombList),countSks(CombList,0,MaxSks)." +
    "getMatkulCombination(List,MaxSks) :- bisa_ambil(X),getMatkulCombination(X,List,MaxSks).";

export const ScheduleGenerator =
    "cekJam([],_) :- !.\n" +
    "cekJam(_,[]) :- !.\n" +
    "cekJam(ListJam,[H|T]) :- helper(H,ListJam), cekJam(ListJam,T).\n" +
    "\n" +
    "helper(_,[]) :- !.\n" +
    "helper([Start,End], [H|T]) :- Start =.. [Day,HStart,MStart], End =.. [Day,HEnd,MEnd], \\+ H =.. [Day,_,_],!,helper([Start,End],T).\n" +
    "helper([Start,End], [H|T]) :- Start =.. [Day,HStart,MStart], End =.. [Day,HEnd,MEnd], \\+ jam(Day,HStart,MStart,HEnd,MEnd,H), helper([Start,End],T).\n" +
    "\n" +
    "addJam(Res,[],Res) :- !.\n" +
    "addJam(ListJam, [[Start,End]|T],Res) :- addJam([Start,End|ListJam],T,Res). \n" +
    "\n" +
    "kumpulanJam([],Res,_,Res) :- !.\n" +
    "kumpulanJam([H|T],AccJadwal,AccJam, Res):- jadwal(H, Kelas, Jadwal), cekJam(AccJam,Jadwal), NewAccJadwal=[[Jadwal, [H,Kelas]]|AccJadwal], addJam(AccJam,Jadwal,NewAccJam), kumpulanJam(T,NewAccJadwal,NewAccJam,Res).\n" +
    "\n" +
    "generateHasil([],Res,Res).\n" +
    "generateHasil([[_, Kelas]|T],TempList, X):- generateHasil(T,[Kelas|TempList], X).\n" +
    "generateJadwal(Matkul, Res):- kumpulanJam(Matkul,[],[], NewRes), generateHasil(NewRes,[], Res)."