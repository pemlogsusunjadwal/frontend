import {RuleListIlkom, RuleListSI} from "../../ai_engine/course_rule";

const GetMatkul = (jurusan) => {
    const list = [];

    if(jurusan === "Ilmu Komputer (Computer Science)"){
        RuleListIlkom.forEach( obj => {
            list.push(...Object.keys(obj))
        })
    }else{
        RuleListSI.forEach( obj => {
            list.push(...Object.keys(obj))
        })
    }
    return list;
}

export default GetMatkul;