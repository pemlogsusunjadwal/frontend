import Core from "@dipta004/tau-prolog-mod-react";
import Lists from "@dipta004/tau-prolog-mod-react/modules/lists"
import CourseRule from "./course_rule";
import {CourseTaken} from "./course_taken";
import {CourseCombinationRule, ScheduleTimeRule, ScheduleGenerator} from "./schedule_time";

Lists(Core);

const Module = ":- use_module(library(lists))."

export const CourseSession = Core.create(2000000);

let courseProgram;

export const MakeProgram = (listMatkul,jurusan) => {
    const program = Module + ((listMatkul.length === 0) ? "sudah_ambil(none)." : CourseTaken(listMatkul)) + CourseRule(jurusan);

    courseProgram = CourseSession.consult(
        program
    );

    if (!courseProgram){
        console.log(courseProgram);
    }
};

export const MakeProgramMatkulSuggestion = (listMatkul,generatedCourse) => {
    const bisaAmbil = `bisa_ambil(${JSON.stringify(listMatkul).replace(/'/g,"").replace(/"/g,"'")}).`
    const program = Module + generatedCourse + bisaAmbil + CourseCombinationRule;

    courseProgram = CourseSession.consult(
        program
    );

    if (!courseProgram){
        console.log(courseProgram);
    }
}

export const MakeProgramGenerateSchedule = (generatedCourse) => {
    const program = Module + generatedCourse + ScheduleTimeRule + ScheduleGenerator;
    console.log(generatedCourse.replace(/\./g,".\n"))
    courseProgram = CourseSession.consult(
        program
    )

    if (!courseProgram){
        console.log(courseProgram);
    }
}

export const CreateQuery = query => {
  CourseSession.query(query);
};

export const GetAnswers = (func) => {
    CourseSession.answers( value => {
        if(value !== false){
            func(value)
        }
    })
}

export const GetAnswersAsync = (func,doneFunc) => {
    CourseSession.answers( value => {
        if(value !== false){
            func(value)
        }else {
            doneFunc();
        }
    })
}

export const GetAnswer = (func) => {
    CourseSession.answer(value => {
        func(value)
    });
}

const arrayRec = (array,term) => {
    if(term.args.length !== 0){
        if(term.args[0].is_float !== undefined){
            array.push(term.args[0].value)
        }else if(term.args[0].id === "."){
                const tempList = [];
                arrayRec(tempList,term.args[0]);
                array.push(tempList);
        }else{
            array.push(term.args[0].id)
        }
        arrayRec(array,term.args[1])
    }
  }

export const FormatAnswer = ans => {
  return Core.format_answer(ans);
};

export const FormatVariabel = ans => {
  return Core.format_variable(ans);
};

export const FormatList = val => {
  const list = [];
  arrayRec(list, val);
  return list;
};
