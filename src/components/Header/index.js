import React from "react";
import { connect } from "react-redux";

import { HeaderContainer, CenterBox } from "./style";

import { isLoggedIn, getName } from "../../modules/auth/selectors";
import LogoutButton from "../LogoutButton";
import LoginButton from "../LoginButton";
import { Link } from "react-router-dom";
import Button from "../Button";

class Header extends React.Component {
  render() {
    const { name, isAuthenticated } = this.props;
    return (
      <HeaderContainer className="wrapper-container center-align-box">
        {isAuthenticated ? (
          <CenterBox>
            <Link to="/">
              <Button value="Home" />
            </Link>
            <span style={{ marginRight: "10px" }}>{name}</span>
            <LogoutButton />
          </CenterBox>
        ) : (
          <CenterBox>
            <LoginButton />
          </CenterBox>
        )}
      </HeaderContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthenticated: isLoggedIn(state),
    name: getName(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
