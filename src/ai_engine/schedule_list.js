export const ScheduleList = (list) => {
    const jadwal  = list.map(obj => {
        return `jadwal('${obj.name}', '${obj.class}', ${
            JSON.stringify(obj.schedule.map(
                    lst => lst.map(
                        lstInside => lstInside.replace(".",",")
                    )
                )
            )}).`.replace(/"/g, "");
    }).join("");

    const sks = list.map(obj => {
        return `sks('${obj.name}', ${obj.credit}).`;
    })

    const uniqueSks = new Set(sks);
    let stringSks = ""
    uniqueSks.forEach(sks => {
        stringSks = `${stringSks}${sks}`
    })

    return `${jadwal}${stringSks}`;
}
