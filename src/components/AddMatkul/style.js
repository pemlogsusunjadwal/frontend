import styled from 'styled-components';

export const AddMatkulContainer = styled.div`
  padding: 5px;
  flex-direction: row;
  display: flex;
  justify-content: center;
  align-items: center;

  > div {
    min-height: 100vh;
    width: 100%;
    justify-content: flex-start;
  }

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const MatkulContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;


