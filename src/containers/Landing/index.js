import React from "react";
import { connect } from "react-redux";

import { LandingContainer } from "./style";
import { isLoggedIn, getJurusan } from "../../modules/auth/selectors";
import AddMatkul from "../../components/AddMatkul";
import schedule from "../../modules/schedule/thunk"


class Landing extends React.Component {
  
  componentDidUpdate = prevProps => {
    const { getCourses, account } = this.props;
    if (account && prevProps.account !== account) {
      getCourses(account.ui_sso_fac_code, "2019-2");
    }
  }
  
  render() {
    const { isAuthenticated, jurusan, course } = this.props;

    return (
      <LandingContainer className="wrapper-container center-align-box">
        {isAuthenticated ? <AddMatkul course={course} jurusan={jurusan} /> : <h1>Halo, Selamat Datang</h1>}
      </LandingContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    account: state.auth.account,
    isAuthenticated: isLoggedIn(state),
    jurusan: getJurusan(state),
    course: state.schedule.siakData.courses,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getCourses: (kdOrg, period) => {
      dispatch(schedule.getCoursesThunk(kdOrg, period));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
