import React from "react";
import { ClassContainer } from "./style";

class Class extends React.Component {
  render() {
    const { name, lecturer, schedule } = this.props;
    return (
      <ClassContainer>
        <div className="container column">
          <div className="container">
            <span style={{ marginRight: "10px" }}>{name}</span>
            <div className="container-no-padding column">
              {lecturer.map(c => {
                return <span style={{ marginRight: "10px" }}>{c}</span>;
              })}
            </div>
            <div className="container-no-padding column">
              {schedule.map(s => {
                return (
                  <span>
                    Ruang {s.room}/{s.day}/{s.start}-{s.end}
                  </span>
                );
              })}
            </div>
          </div>
        </div>
      </ClassContainer>
    );
  }
}

export default Class;
