import React from 'react';

import {connect} from "react-redux";
import { Link } from "react-router-dom";
import { AddMatkulContainer, MatkulContainer } from './style';
import GetMatkul from "./listMatkul";
import {CreateQuery, FormatList, GetAnswer, MakeProgram} from "../../ai_engine/prolog";
import {RuleListIlkom, RuleListSI} from "../../ai_engine/course_rule";
import {SetJurusan} from "../../globalActions";
import MatkulCard from './MatkulCard';
import Button from '../Button';
import PickedCard from './PickedCard';
import Modal from '../Modal';

class AddMatkul extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        pick : [],
        choice : [],
        activePick : "",
        nextCourse : [],
        isModalOpen: false
    }
  }

  componentDidMount() {
      this.setState({
          choice:GetMatkul(this.props.jurusan)
      })
  }


  changeMatkul = (e) => {
    this.setState({
      activePick : e.target.value
    })
  }

  checkDependencies = matkulPicked => {
      const dependencies = [];
      if(this.props.jurusan === "Ilmu Komputer (Computer Science)"){
          RuleListIlkom.filter(
              obj => Object.keys(obj).includes(matkulPicked)
          ).forEach(obj => {
              dependencies.push(obj[matkulPicked]);
          })
      }else{
          RuleListSI.filter(
              obj => Object.keys(obj).includes(matkulPicked)
          ).forEach(obj => {
              dependencies.push(obj[matkulPicked]);
          })
      }
      return dependencies.map(obj => {
            return obj.filter(val => !this.state.pick.includes(val)).join(", ")
          }).join(" or ");
  }

  addMatkul = matkulPicked => {
      if(this.state.choice.includes(matkulPicked)){
          MakeProgram(this.state.pick,this.props.jurusan);
          CreateQuery(`findall(X,generate_matkul(X),Y),member('${matkulPicked}',Y).`.toString());
          GetAnswer(val => {
              if(val !== false){
                  const list = this.state.choice.filter(obj => obj !== matkulPicked);
                  const pickChoice = [...this.state.pick,matkulPicked];
                  this.setState({
                      pick : pickChoice,
                      choice : list,
                      activePick:""
                  })
              }else{
                  alert(`Course ${matkulPicked} need course ${this.checkDependencies(matkulPicked)}`)
                  this.setState({
                      activePick:""
                  })
              }
          })
      }
  }

  removeMatkul = (matkul) => {
    this.setState({
      pick : this.state.pick.filter(obj => obj !== matkul),
      choice : [matkul,...this.state.choice]
    })
  }

  getNextCourse = () => {
       MakeProgram(this.state.pick,this.props.jurusan);
    CreateQuery("findall(X,generate_matkul(X),Y).");
      GetAnswer((val) => {
        const list = FormatList(val.links.Y);

        this.setState({
          nextCourse : list,
          isModalOpen: true
        })
      })
  }

  closeModal = ()  => {
    this.setState({
      isModalOpen: false
    })
  }


  renderMatkul = () => {
    const { choice } = this.state;
    return choice.map((data, count) => {
      return <MatkulCard data={data} key={count} handler={this.addMatkul} />
    })
  }

  render() {
    const { pick, isModalOpen, nextCourse } = this.state;

    return (
      <AddMatkulContainer>
        {
          isModalOpen && 
          <Modal>
              <h1>Courses you can enroll</h1>
               {nextCourse.map((obj,idx) => {
                 return <h2>{`${idx+1}. ${obj}`}</h2>
               })}
            <Button value="Close" onClick={this.closeModal} />
            <Link to={{
              pathname:"/schedule",
              state:{
                "listMatkul": nextCourse
              }
            }}>Generate</Link>
          </Modal>
        }
        <div>
          <h1>Select Your Picked Course</h1>
          <MatkulContainer>
            {this.renderMatkul()}
          </MatkulContainer>
        </div>
        <div>
          <MatkulContainer style={{alignItems:'center'}}>
            <h1>Your Picked Courses</h1>
            {
              pick.length > 0 && 
            <Button value="Generate Schedule" onClick={this.getNextCourse} />
            }
          </MatkulContainer>
          <MatkulContainer>
            {this.state.pick.map((val,ind) => {
              return (
              <PickedCard data={val} handler={() => this.removeMatkul(val)} />)
            })}
          </MatkulContainer>
        </div>
      </AddMatkulContainer>
    );
  }
}

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        setJurusan : jurusan => dispatch(SetJurusan(jurusan))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AddMatkul);
