export function getUserToken(state) {
  return state.auth.token;
}

export function getName(state) {
  if (state.auth.account) {
    return state.auth.account.name;
  }
  return undefined;
}

export function isLoggedIn(state) {
  const token = getUserToken(state);
  return !!token;
}

export const getJurusan = state => {

  if (state.auth.account) {
    return state.auth.account.ui_sso_study_program;
  }
  return undefined;
}