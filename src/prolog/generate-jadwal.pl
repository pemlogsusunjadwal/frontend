jadwal('Analisis Numerik', 'Anum - A', [[senin(08,00),senin(09,40)],[rabu(08,00),rabu(08,50)]]).
jadwal('Analisis Numerik', 'Anum - B', [[senin(10,00),senin(11,40)],[rabu(09,00),rabu(09,50)]]).
jadwal('Analisis Numerik', 'Anum - C', [[senin(08,00),senin(09,40)],[rabu(08,00),rabu(08,50)]]).
jadwal('Basis Data', 'Basis Data - A', [[selasa(08,00),selasa(09,40)],[kamis(08,00),kamis(09,40)]]).
jadwal('Basis Data', 'Basis Data - B', [[selasa(08,00),selasa(09,40)],[kamis(08,00),kamis(09,40)]]).
jadwal('Basis Data', 'Basis Data - C', [[selasa(08,00),selasa(09,40)],[kamis(08,00),kamis(09,40)]]).
jadwal('Basis Data', 'Basis Data - D', [[selasa(10,00),selasa(11,40)],[kamis(10,00),kamis(11,40)]]).
jadwal('Basis Data', 'Basis Data - E', [[selasa(10,00),selasa(11,40)],[kamis(10,00),kamis(11,40)]]).
jadwal('Desain & Analisis Algoritma', 'DAA', [[selasa(13,00),selasa(14,40)],[kamis(13,00),kamis(14,40)]]).
jadwal('Metodologi Penelitian & Penulisan Ilmiah', 'MPPI-A', [[rabu(10,00),rabu(11,40)],[jumat(10,00),jumat(10,50)]]).
jadwal('Metodologi Penelitian & Penulisan Ilmiah', 'MPPI-B', [[rabu(10,00),rabu(11,40)],[jumat(10,00),jumat(10,50)]]).
jadwal('Metodologi Penelitian & Penulisan Ilmiah', 'MPPI-C', [[selasa(17,00),selasa(17,50)],[jumat(08,00),jumat(09,40)]]).
jadwal('Metodologi Penelitian & Penulisan Ilmiah', 'MPPI-D', [[rabu(10,00),rabu(11,40)],[jumat(10,00),jumat(10,50)]]).
jadwal('Metodologi Penelitian & Penulisan Ilmiah', 'MPPI-E', [[kamis(10,00),kamis(11,40)],[jumat(08,00),jumat(08,50)]]).
jadwal('Metodologi Penelitian & Penulisan Ilmiah', 'MPPI-F', [[kamis(10,00),kamis(11,40)],[jumat(08,00),jumat(08,50)]]).
jadwal('Pemrograman Lanjut', 'Advanced Program - A', [[selasa(08,00),selasa(09,40)],[kamis(08,00),kamis(09,40)],[kamis(15,00),kamis(16,40)]]).
jadwal('Pemrograman Lanjut', 'Advanced Program - B', [[selasa(10,00),selasa(11,40)],[kamis(10,00),kamis(11,40)],[kamis(15,00),kamis(16,40)]]).
jadwal('Pemrograman Lanjut', 'Advanced Program - C', [[selasa(08,00),selasa(09,40)],[kamis(08,00),kamis(09,40)],[kamis(15,00),kamis(16,40)]]).
jadwal('Sistem Operasi', 'OS - A', [[senin(08,00),senin(09,40)],[rabu(08,00),rabu(09,40)]]).
jadwal('Sistem Operasi', 'OS - B', [[senin(10,00),senin(11,40)],[rabu(10,00),rabu(11,40)]]).
jadwal('Sistem Operasi', 'OS - C', [[senin(13,00),senin(14,40)],[rabu(13,00),rabu(14,40)]]).
jadwal('Sistem Operasi', 'OS - D', [[senin(10,00),senin(11,40)],[rabu(10,00),rabu(11,40)]]).
jadwal('Sistem Operasi', 'OS - E', [[senin(08,00),senin(09,40)],[rabu(08,00),rabu(09,40)]]).
jadwal('Statistika dan Probabilitas', 'Statprob-A', [[senin(08,00),senin(09,40)],[rabu(08,00),rabu(08,50)]]).
jadwal('Statistika dan Probabilitas', 'Statprob-B', [[senin(08,00),senin(09,40)],[rabu(08,00),rabu(08,50)]]).
jadwal('Statistika dan Probabilitas', 'Statprob-C', [[senin(08,00),senin(09,40)],[rabu(08,00),rabu(08,50)]]).
jadwal('Statistika dan Probabilitas', 'Statprob-D', [[senin(10,00),senin(11,40)],[rabu(10,00),rabu(10,50)]]).
jadwal('Statistika dan Probabilitas', 'Statprob-E', [[senin(10,00),senin(11,40)],[rabu(10,00),rabu(10,50)]]).
jadwal('Teori Bahasa & Automata', 'TBA-A', [[senin(13,00),senin(14,40)],[rabu(14,00),rabu(15,40)]]).
jadwal('Teori Bahasa & Automata', 'TBA-B', [[senin(13,00),senin(14,40)],[rabu(14,00),rabu(15,40)]]).
jadwal('Teori Bahasa & Automata', 'TBA-C', [[senin(15,00),senin(16,40)],[selasa(15,00),selasa(16,40)]]).

sks('Analisis Numerik', 3).
sks('Basis Data', 4).
sks('Desain & Analisis Algoritma', 4).
sks('Metodologi Penelitian & Penulisan Ilmiah', 3).
sks('Pemrograman Lanjut', 4).
sks('Sistem Operasi', 4).
sks('Statistika dan Probabilitas', 3).
sks('Teori Bahasa & Automata', 4).

jam(Day,H_Start,M_Start,H_End,_, Res) :- H_Start < H_End,Res =.. [Day,H_Start,M_Start].
jam(Day, H_Start,M_Start,H_Start,M_End,Res) :- M_Start =< M_End, Res =.. [Day,H_Start,M_Start].
jam(Day, H_Start,M_Start,H_Start,M_End, Res) :- M_Start < M_End,!, M_Start < 59, M_Temp is M_Start+1, jam(Day, H_Start,M_Temp,H_Start,M_End,Res).
jam(Day, H_Start,M_Start,H_End,M_End, Res) :- H_Start < H_End, M_Start < 59,!, M_Temp is M_Start+1, jam(Day,H_Start,M_Temp,H_End,M_End,Res). 
jam(Day, H_Start,_,H_End,M_End, Res) :- H_Start < H_End, H_Temp is H_Start+1, jam(Day,H_Temp,0,H_End,M_End,Res).

cekJam([],_) :- !.
cekJam(_,[]) :- !.
cekJam(ListJam,[H|T]) :- helper(H,ListJam), cekJam(ListJam,T).

helper(_,[]) :- !.
helper([Start,End], [H|T]) :- Start =.. [Day,HStart,MStart], End =.. [Day,HEnd,MEnd], \+ H =.. [Day,_,_],!,helper([Start,End],T).
helper([Start,End], [H|T]) :- Start =.. [Day,HStart,MStart], End =.. [Day,HEnd,MEnd], \+ jam(Day,HStart,MStart,HEnd,MEnd,H), helper([Start,End],T).

addJam(Res,[],Res) :- !.
addJam(ListJam, [[Start,End]|T],Res) :- addJam([Start,End|ListJam],T,Res). 

kumpulanJam([],Res,_,Res) :- !.
kumpulanJam([H|T],AccJadwal,AccJam, Res):- jadwal(H, Kelas, Jadwal), cekJam(AccJam,Jadwal), NewAccJadwal=[[Jadwal, [H,Kelas]]|AccJadwal], addJam(AccJam,Jadwal,NewAccJam), kumpulanJam(T,NewAccJadwal,NewAccJam,Res).

generateHasil([],Res,Res).
generateHasil([[_, Kelas]|T],TempList, X):- generateHasil(T,[Kelas|TempList], X).
generateJadwal(Matkul, Res):- kumpulanJam(Matkul,[],[], NewRes), generateHasil(NewRes,[], Res).