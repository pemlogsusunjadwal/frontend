# Frontend for Pemlog assignment
###
Repository dari frontend project susun jadwal ng.
hasil project bisa dilihat di https://susun-jadwal-ng.netlify.app.

# Instalasi
###
- install node js terlebih dahulu apabila belum mengintsall nya
    - https://nodejs.org/en/download/package-manager/
- lalu clone repository ini
    ```
    git clone https://gitlab.com/pemlogsusunjadwal/frontend.git
    ```
- masuk ke directory frontend
    ```
    cd frontend
    ```
- install semua dependencies yang ada
    ```
    npm install
    ```
- jalankan server node js
    ```
    npm start
    ```
- server sudah berjalan di [http://localhost:3000](http://localhost:3000)

**NOTE: saat menjalankan server ini, jangan lupa untuk menjalankan server backend, silahkan clone repo nya di https://gitlab.com/pemlogsusunjadwal/backend.git**

# Penjelasan singkat Implementasi Prolog
### Kami menggunakan Tau Prolog sebagai interpreter Prolog di project kami, http://tau-prolog.org/.
### file prolog yang di demo kan di penjelasan ini ada di frontend/src/prolog
- dalam implementasi nya, secara garis besar kami menggunakan 3 code base prolog, yaitu;
    - <b>Generate mata kuliah yang bisa diambil - <i>next-matkul.pl</i></b>. pada file ini sudah kami tuliskan beberapa database knowledge mata kuliah apa yang sudah diambil, dan dengan data tersebut dengan rule rule prasyarat mata kuliah, akan menghasilkan mata kuliah yang bisa diambil.
        ```
        ?- generate_matkul(X).
        X = 'Basis Data' ;
        X = 'Statistika dan Probabilitas' ;
        X = 'Metodologi Penelitian & Penulisan Ilmiah' ;
        X = 'Sistem Operasi' ;
        X = 'Pemrograman Lanjut' ;
        X = 'Teori Bahasa & Automata' ;
        X = 'Rekayasa Perangkat Lunak' ;
        X = 'Analisis Numerik' ;
        X = 'Desain & Analisis Algoritma'.
        ```
    - <b>Mencari semua kombinasi mata kuliah apa saja yang bisa diambil berdasarkan jumlah sks yang diinginkan - <i>kombinasi-matkul.pl</i></b>. pada file ini kami mencari semua kombinasi mata kuliah apa saja yang dapat diambil berdasarkan jumlah sks yang diambil. untuk menggunakan file ini, kami harus generate mata kuliah terlebih dahulu dari file <i>next-matkul</i> lalu hasilnya akan digunakan untuk query.
        ```
        ?- getMatkulCombination(X, 23).
        X = ['Analisis Numerik', 'Basis Data', 'Desain & Analisis Algoritma', 'Pemrograman Lanjut', 'Sistem Operasi', 'Teori Bahasa & Automata'] ;
        X = ['Basis Data', 'Desain & Analisis Algoritma', 'Metodologi Penelitian & Penulisan Ilmiah', 'Pemrograman Lanjut', 'Sistem Operasi', 'Teori Bahasa & Automata'] ;
        X = ['Basis Data', 'Desain & Analisis Algoritma', 'Pemrograman Lanjut', 'Sistem Operasi', 'Statistika dan Probabilitas', 'Teori Bahasa & Automata'] ;
        ```
    - <b>Generate semua kemungkinan kelas yang bisa diambil berdasarkan hari dan jam dari kelas sebuah mata kuliah - <i>generate-jadwal.pl</i>. pada file ini kami memasukan database jadwal mata kuliah yang kami dapat dari SIAKNG lalu di parse ke file prolog menjadi sebuah fakta. contoh hasilnya ada di file ini. untuk mendapatkan hasil yang diinginkan dibutuhkan hasil query dari file sebelumnya untuk query.
        ```
        ?- generateJadwal(['Analisis Numerik', 'Basis Data', 'Desain & Analisis Algoritma', 'Pemrograman Lanjut', 'Sistem Operasi', 'Teori Bahasa & Automata'], X).
        X = [['Analisis Numerik', 'Anum - A'], ['Basis Data', 'Basis Data - A'], ['Desain & Analisis Algoritma', 'DAA'], ['Pemrograman Lanjut', 'Advanced Program - B'], ['Sistem Operasi', 'OS - B'], ['Teori Bahasa & Automata', 'TBA-A']] ;
        X = [['Analisis Numerik', 'Anum - A'], ['Basis Data', 'Basis Data - A'], ['Desain & Analisis Algoritma', 'DAA'], ['Pemrograman Lanjut', 'Advanced Program - B'], ['Sistem Operasi', 'OS - B'], ['Teori Bahasa & Automata', 'TBA-B']] ;
        X = [['Analisis Numerik', 'Anum - A'], ['Basis Data', 'Basis Data - A'], ['Desain & Analisis Algoritma', 'DAA'], ['Pemrograman Lanjut', 'Advanced Program - B'], ['Sistem Operasi', 'OS - B'], ['Teori Bahasa & Automata', 'TBA-C']] ;
        -------------- terlalu banyak ------------
        ```

