import React from 'react';
import {Container} from './style'
import Button from '../../Button'

const PickedCard = props => {

    const onClick = () => {
        props.handler(props.data);
    }

    return (
        <Container>
            <h5>{props.data}</h5>
            <Button onClick={() => onClick()} value="Remove"/>
        </Container>
    )
}

export default PickedCard;