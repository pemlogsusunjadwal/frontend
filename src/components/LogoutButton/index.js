import React from "react";
import { connect } from "react-redux";
import { LogoutButtonContainer } from "./style";
import auth from "../../modules/auth/thunk";
import Button from "../Button";

class LogoutButton extends React.Component {
  onClick = () => {
    const { logout } = this.props;
    logout();
  };

  render() {
    return (
      <LogoutButtonContainer>
        <Button onClick={this.onClick} value="Logout" />
      </LogoutButtonContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => {
      dispatch(auth.logoutThunk());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LogoutButton);
