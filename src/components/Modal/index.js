import React from "react";
import {Container, BlackDiv, Content} from "./style"

const Modal = props => {
    return (
        <Container>
            <BlackDiv />
            <Content>
                {props.children}
            </Content>
        </Container>
    )
}

export default Modal;