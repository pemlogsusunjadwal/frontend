import styled from "styled-components";

export const HeaderContainer = styled.header``;

export const CenterBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
`;
