import {SET_COURSES, LOADING_COURSES, GENERATE_SCHEDULE, LIST_MATKUL_SCHEDULE_EXISTS} from "./constant";

const INITIAL_STATE = {
  siakData: {
    courses: [],
    period: undefined
  },
  loading: false,
  nextRawCourses: "",
  matkulExists:[],
};

export default function scheduleReducer(state, action) {
  switch (action.type) {
    case SET_COURSES:
      return {
        ...state,
        siakData: {
          courses: action.payload.courses[0],
          period: action.payload.period
        },
        loading: false
      };
    case LOADING_COURSES:
      return {
        ...state,
        loading: true
      };
    case GENERATE_SCHEDULE:
      return {
        ...state,
        nextRawCourses: action.payload,
        loading: false
      }
    case LIST_MATKUL_SCHEDULE_EXISTS:
      return {
        ...state,
        matkulExists: action.payload
      }
    default:
      return state === undefined ? INITIAL_STATE : state;
  }
}
