import spideyApi from "../../api/spidey";
import { coursesActions } from "./constant";
import { ScheduleList } from "../../ai_engine/schedule_list";

const schedule = {
  getCoursesThunk: (kdOrg, period) => {
    return dispatch => {
      dispatch(coursesActions.loading());
      let body
      if (kdOrg) {
        body = spideyApi.schedule.getCourses(kdOrg, period);
      } else {
        body = spideyApi.schedule.getCourses("01.00.12.01", period)
      }
      body
        .then(res => {
          dispatch(coursesActions.set(res.data));
        })
        .catch(err => {
          console.log(err);
        });
    };
  },
  generateScheduleCombinationThunk: (listMatkul, course) => {
    return dispatch => {
      dispatch(coursesActions.loading())
      const cleanedData = cleaningCoourse(listMatkul, course);
      const courseName = [...new Set(cleanedData.map(obj => obj.name))];
      dispatch(coursesActions.generate(ScheduleList(cleanedData)))
      dispatch(coursesActions.matkulExists(courseName));
    }
  },
};

export default schedule;

const cleaningCoourse = (listMatkul, course) => {

  const cleanedData = []
  course.filter(c => {
    return listMatkul.includes(c.name)
  }).map(c => {
    return c.classes.map(a => {
      return {
        name: c.name,
        class: a.name,
        credit: c.credit,
        schedule: a.schedule_items.map(s => {
          const res = []
          const start = `${s.day.toLowerCase()}(${s.start})`
          const end = `${s.day.toLowerCase()}(${s.end})`
          res.push(start)
          res.push(end)
          return res;
        })
      }
    })
  }).forEach(e => {
    e.forEach(a => {
      cleanedData.push(a)
    })
  });

  return cleanedData;
}