import styled from "styled-components";

export const Container = styled.section`
position: fixed;
display: flex;
justify-content: center;
align-items: center;
bottom: 0;
height: 100vh;
width: 100vh;
`;

export const BlackDiv = styled.div`
    width: 200vw;
    height: 200vh;
    position: absolute;
    z-index: 5;
    background-color: black;
    opacity: 0.5;
`

export const Content = styled.div`
    background-color: white;
    width: 100vw;
    height:  90vh;
    z-index: 10;
    padding: 10px;
    overflow-y: scroll;
`