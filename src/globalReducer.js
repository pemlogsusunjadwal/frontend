import {CHANGE_JURUSAN} from "./globalConstants";

const initialState = {
  jurusan:"Ilmu Komputer (Computer Science)"
};

function globalReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_JURUSAN:
      return {
        ...state,
        jurusan:action.payload
      };
    default:
      return state;
  }
}

export default globalReducer;
