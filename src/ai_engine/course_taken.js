export const CourseTaken = (list) => {
    // return "sudah_ambil('Dasar-dasar Pemrograman 1')."
    return list.map(obj => {
        return `sudah_ambil('${obj}').`;
    }).join("");
}
