import React from "react";
import { connect } from "react-redux";

import { CoursesContainer } from "./style";
import schedule from "../../modules/schedule/thunk";
import Course from "../Course";

class Courses extends React.Component {
  componentDidUpdate = prevProps => {
    const { account, getCourses } = this.props;
    if (account && account !== prevProps.account) {
      getCourses(account.ui_sso_fac_code, "2019-2");
    }
  };

  renderCourse = () => {
    const { course } = this.props;
    if (course) {
      const courses = course.map(c => {
        return (
          <Course
            name={c.name}
            credit={c.credit}
            term={c.term}
            classes={c.classes}
          />
        );
      });
      return courses;
    }
    return [];
  };

  render() {
    const { period } = this.props;
    return (
      <CoursesContainer>
        <div className="container center-align-box">
          <h2>{period}</h2>
        </div>
        <div className="container column">{this.renderCourse()}</div>
      </CoursesContainer>
    );
  }
}

const mapStateToProps = state => {

  return {
    account: state.auth.account,
    course: state.schedule.siakData.courses,
    period: state.schedule.siakData.period
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCourses: (kdOrg, period) => {
      dispatch(schedule.getCoursesThunk(kdOrg, period));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Courses);
