export const SET_COURSES = "schedule/SET_COURSES";
export const LOADING_COURSES = "schedule/LOADING_COURSES";
export const GENERATE_SCHEDULE = "schedule/GENERATE_SCHEDULE";
export const LIST_MATKUL_SCHEDULE_EXISTS = "schedule/LIST_MATKUL_SCHEDULE_EXISTS";

export const coursesActions = {
  loading: () => ({
    type: LOADING_COURSES
  }),
  set: result => ({
    type: SET_COURSES,
    payload: result
  }),
  generate: data => ({
    type: GENERATE_SCHEDULE,
    payload: data
  }),
  matkulExists: data => ({
    type: LIST_MATKUL_SCHEDULE_EXISTS,
    payload: data
  })
};
