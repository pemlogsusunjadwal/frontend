import React from "react";
import { CourseContainer } from "./style";
import Class from "../Class";

class Course extends React.Component {
  renderClass = () => {
    const { classes } = this.props;

    const classList = classes.map(c => {
      return (
        <Class
          name={c.name}
          lecturer={c.lecturer}
          schedule={c.schedule_items}
        />
      );
    });

    return classList;
  };

  render() {
    const { name, term, credit } = this.props;
    return (
      <CourseContainer>
        <div className="container column">
          <div className="container">
            <span>Nama: {name},</span>
            <span>SKS: {credit},</span>
            <span>Semester: {term}</span>
          </div>
          <div>{this.renderClass()}</div>
        </div>
      </CourseContainer>
    );
  }
}

export default Course;
