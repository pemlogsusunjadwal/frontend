sks('Analisis Numerik', 3).
sks('Basis Data', 4).
sks('Desain & Analisis Algoritma', 4).
sks('Metodologi Penelitian & Penulisan Ilmiah', 3).
sks('Pemrograman Lanjut', 4).
sks('Sistem Operasi', 4).
sks('Statistika dan Probabilitas', 3).
sks('Teori Bahasa & Automata', 4).

bisa_ambil(['Analisis Numerik','Basis Data','Desain & Analisis Algoritma','Metodologi Penelitian & Penulisan Ilmiah','Pemrograman Lanjut','Sistem Operasi','Statistika dan Probabilitas','Teori Bahasa & Automata']).

comb2(_,[]).
comb2([X|T],[X|Comb]):-comb2(T,Comb).
comb2([_|T],[X|Comb]):-comb2(T,[X|Comb]).

countSks([],Max,Max).
countSks([H|T],Before,Max) :- sks(H,Val), NewTotal is Before + Val, NewTotal =< Max, countSks(T,NewTotal,Max).

getMatkulCombination(X,CombList,MaxSks) :- comb2(X,CombList),countSks(CombList,0,MaxSks).
getMatkulCombination(List,MaxSks) :- bisa_ambil(X),getMatkulCombination(X,List,MaxSks).