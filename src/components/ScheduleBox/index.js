import React, { useState, useEffect } from "react";
import {Container, ScheduleContainer} from "./style"
import { useSelector } from "react-redux";

const ScheduleBox = props => {

    const [pickedRes, setPickedRes] = useState(0)
    const [res, setRes] = useState({
        senin: [],
        selasa: [],
        rabu: [],
        kamis: [],
        jumat: [],
        sabtu: []
    })
    const { course } = useSelector(state => ({
        course: state.schedule.siakData.courses,
    }))

    const generateResult = () => {

        const res = props.scheduleGenerated[pickedRes].map(a => {
            return course.filter(c => {
                return c.name === a[0]
            }).map(f => {
                return {
                    name : f.name,
                    classes : f.classes.filter(d => {
                        return d.name === a[1]
                    })[0]
                }
            })[0]
        })

        const fixRes = {
            senin: [],
            selasa: [],
            rabu: [],
            kamis: [],
            jumat: [],
            sabtu: []
        }

        res.map(a => {
            return {
                name: a.name,
                className : a.classes.name,
                lecturer: a.classes.lecturer,
                time: a.classes.schedule_items,
            }
        }).map(a => {
            a.time.forEach(t => {
                const mapData = {
                    name: a.name,
                    className : a.className,
                    lecturer: a.lecturer,
                    time: t
                }
                switch(t.day) {
                    case "Senin":
                        return fixRes.senin.push(mapData)
                    case "Selasa":
                        return fixRes.selasa.push(mapData)
                    case "Rabu":
                        return fixRes.rabu.push(mapData)
                    case "Kamis":
                        return fixRes.kamis.push(mapData)
                    case "Jumat":
                        return fixRes.jumat.push(mapData)
                    case "Sabtu":
                        return fixRes.sabtu.push(mapData)
                    default:
                        return null
                }
            });
        })
        const compareTime = (a,b) => Number(a.time.start) - Number(b.time.start);
        Object.values(fixRes).forEach(obj => obj.sort(compareTime))
        setRes(fixRes)
    }

    useEffect(() => {
        generateResult();
    }, [pickedRes])

    const changePickedRes = num => {
        // num is +1 / -1
        if (pickedRes === 0 && num === -1) {
            return null
        }
        if (pickedRes === props.resultCount - 1 && num === 1) {
            return null
        }
        return setPickedRes(pickedRes + num)
    }

    const generateDay = day => {
        if (day.length === 0) {
            return <h3>Hari Kosong :)</h3>
        }
        return day.map(d => {
            return (
                <div>
                    <h3>{d.name} - ({d.className})</h3>
                    <p>Dosen: {`${d.lecturer.map(l => {
                        return `${l} `
                    })}`}</p>
                    <p>Jam: {`${d.time.start} - ${d.time.end}`}</p>
                    <p>Ruangan: {d.time.room}</p>                    
                </div>
            )
        })
    }

    return (
        <Container>
            <h1>Jumlah Hasil Kemungkinan {props.resultCount}</h1> 
            {props.done && <h3>Selesai</h3>}
            <div>
                <button onClick={()=> changePickedRes(-1)}>Prev</button>
                <button onClick={()=> changePickedRes(1)}>Next</button>
            </div>
            <h3>Kemungkinan ke - {pickedRes + 1}</h3>
            <ScheduleContainer>
                <h1>Senin</h1>
                {generateDay(res.senin)}
                <h1>Selasa</h1>
                {generateDay(res.selasa)}
                <h1>Rabu</h1>
                {generateDay(res.rabu)}
                <h1>Kamis</h1>
                {generateDay(res.kamis)}
                <h1>Jumat</h1>
                {generateDay(res.jumat)}
                <h1>Sabtu</h1>
                {generateDay(res.sabtu)}
            </ScheduleContainer>
        </Container>
    )
}

export default ScheduleBox;