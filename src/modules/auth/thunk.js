import spideyApi from "../../api/spidey";
import { setAuthToken } from "../../api/http";
import { sessionActions, loadingAccount } from "./constant";

const auth = {
  loginThunk: () => {
    return async dispatch => {
      try {
        dispatch(loadingAccount());
        const body = await spideyApi.auth.SSOLogin();
        setAuthToken(body.token);
        dispatch(sessionActions.setAccount(body.user));
        dispatch(sessionActions.setToken(body.token));
      } catch (error) {
        console.log(error);
      }
    };
  },
  logoutThunk: () => {
    return dispatch => {
      dispatch(sessionActions.clearToken());
      dispatch(sessionActions.clearAccount());
    };
  }
};

export default auth;
