import {CHANGE_JURUSAN} from "./globalConstants";

export const SetJurusan = (jurusan) => {
    return {type:CHANGE_JURUSAN, payload:jurusan}
}