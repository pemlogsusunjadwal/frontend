import { createStore, applyMiddleware } from "redux";
import { routerMiddleware } from "connected-react-router";
import { persistStore } from "redux-persist";
import thunk from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import { composeWithDevTools } from "redux-devtools-extension";
import createRootReducer from "./modules";

export const history = createHistory({
  basename: process.env.PUBLIC_URL
});

const initialState = {};
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

if (process.env.NODE_ENV === "development") {
  const devToolsExtension = window.REDUX_DEVTOOLS_EXTENSION;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = composeWithDevTools(
  applyMiddleware(...middleware),
  ...enhancers
);

export const store = createStore(
  createRootReducer(history),
  initialState,
  composedEnhancers
);

export const persistor = persistStore(store);
