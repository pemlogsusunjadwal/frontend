/* eslint-disable */
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import globalReducer from "./../globalReducer";
import authReducer from "./auth";
import scheduleReducer from "./schedule";

export default history =>
  combineReducers({
    router: connectRouter(history),
    global: globalReducer,
    auth: persistReducer(
      { key: "auth", storage, whitelist: ["token", "account"] },
      authReducer
    ),
    schedule: persistReducer(
      { key: "schedule", storage, whitelist: ["siakData"] },
      scheduleReducer
    ),
});
