import {
  SET_ACCOUNT,
  SET_TOKEN,
  CLEAR_ACCOUNT,
  CLEAR_TOKEN,
  LOADING_ACCOUNT
} from "./constant";

const INITIAL_STATE = {
  token: undefined,
  account: undefined,
  accountIsLoading: false
};

export default function authReducer(state, action) {
  switch (action.type) {
    case SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    case CLEAR_TOKEN:
      return {
        ...state,
        token: null
      };
    case LOADING_ACCOUNT:
      return {
        ...state,
        accountIsLoading: true
      };
    case SET_ACCOUNT:
      return {
        ...state,
        account: { ...action.payload },
        accountIsLoading: false
      };
    case CLEAR_ACCOUNT:
      return {
        ...state,
        account: null
      };
    default:
      return state === undefined ? INITIAL_STATE : state;
  }
}
