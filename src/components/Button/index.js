import React from "react";
import { ButtonContainer } from "./style";

class Button extends React.Component {
  onClick = () => {
    const { onClick } = this.props;
    if (onClick) {
      onClick();
    }
  };

  render() {
    const { value,disabled } = this.props;
    return (
      <ButtonContainer>
        <button disabled={disabled} type="button" onClick={this.onClick}>
          {value}
        </button>
      </ButtonContainer>
    );
  }
}

Button.propTypes = {};

export default Button;
