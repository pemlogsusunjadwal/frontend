const RuleMatkulUmum = {
    "MPK Terintegrasi B": [],
    "MPK Terintegrasi A": [],
    "Fisika Dasar": [],
    "Matematika Diskret 1": [],
    "Dasar-dasar Pemrograman 1": [],
    "MPK Agama Islam": [],
    "MPK Bahasa Inggris": [],
    "Matematika Dasar 1": [],
    "Matematika Diskret 2": [],
    "Dasar-dasar Pemrograman 2": ["Dasar-dasar Pemrograman 1"],
    "Aljabar Linier": ["Matematika Dasar 1"],
    "Perancangan & Pemrograman Web": ["Dasar-dasar Pemrograman 1"],
    "Struktur Data & Algoritma": ["Dasar-dasar Pemrograman 2"],
    "Basis Data": ["Dasar-dasar Pemrograman 2"],
    "Statistika dan Probabilitas": ["Matematika Diskret 1", "Matematika Dasar 1"],
    "Metodologi Penelitian & Penulisan Ilmiah": ["MPK Terintegrasi A", "MPK Terintegrasi B"]
}

const RuleMatkulJurusanIlkom = {
    "Pengantar Sistem Dijital":[],
    "Pengantar Organisasi Komputer":["Pengantar Sistem Dijital"],
    "Sistem Operasi":["Pengantar Organisasi Komputer"],
    "Matematika Dasar 2":["Matematika Dasar 1"],
    "Pemrograman Lanjut":["Perancangan & Pemrograman Web","Dasar-dasar Pemrograman 2"],
    "Teori Bahasa & Automata":["Matematika Diskret 2","Matematika Diskret 1"],
    "Rekayasa Perangkat Lunak":["Dasar-dasar Pemrograman 2"],
    "Pemrograman Sistem":["Sistem Operasi","Struktur Data & Algoritma"],
    "Sistem Cerdas":["Statistika & Probabilitas","Matematika Diskret 1","Struktur Data & Algoritma"],
    "Jaringan Komputer":["Sistem Operasi","Dasar-dasar Pemrograman 1"],
    "Proyek Perangkat Lunak":["Rekayasa Perangkat Lunak","Basis Data"],
    "Analisis Numerik":["Matematika Dasar 2","Aljabar Linier"],
    "Sains Data":["Statistika & Probabilitas","Basis Data"],
    "Desain & Analisis Algoritma":["Struktur Data & Algoritma"],
}

const RuleMatkulJurusanSI = {
    "Prinsip-Prinsip Sistem Informasi": [],
    "Dasar-Dasar Arsitektur Komputer": [],
    "Sistem Operasi": ["Dasar-Dasar Arsitektur Komputer"],
    "Administrasi Bisnis": [],
    "Prinsip-Prinsip Manajemen": [],
    "Sistem-Sistem Perusahaan": ["Administrasi Bisnis"],
    "Sistem Informasi Akuntansi dan Keuangan": ["Administrasi Bisnis"],
    "Sistem Interaksi": [],
    "Analisis dan Perancangan Sistem Informasi": ["Basis Data", "Prinsip-Prinsip Sistem Informasi"],
    "Arsitektur & Pemrograman Aplikasi Perusahaan": ["Perancangan & Pemrograman Web", "Basis Data", "Struktur Data & Algoritma"],
    "Manajemen Proyek TI": ["Prinsip-Prinsip Sistem Informasi", "Prinsip-Prinsip Manajemen"],
    "Jaringan Komunikasi Data": ["Dasar-dasar Pemrograman 1", "Fisika Dasar", "Sistem Operasi"],
    "Proyek Pengembangan Sistem Informasi": ["Sistem Interaksi", "Analisis dan Perancangan Sistem Informasi", "Manajemen Proyek TI", "Arsitektur dan Pemrograman Aplikasi Perusahaan"],
    "Komunikasi Bisnis dan Teknis": [],
    "Statistika Terapan": ["Statistika & Probabilitas"],
    "Manajemen Sistem Informasi": ["Prinsip-Prinsip Sistem Informasi"]
}

// export const MPK_O_S_List = [
//     "MPK Olahraga Bola Basket",
//     "MPK Olahraga Bola Voli",
//     "MPK Olahraga Futsal",
//     "MPK Olahraga Sepakbola",
//     "MPK Olahraga Tenis",
//     "MPK Olahraga Tenis Meja",
//     "MPK Seni Apresiasi Film",
//     "MPK Seni Apresiasi Musik",
//     "MPK Seni Batik",
//     "MPK Seni Fotografi",
//     "MPK Seni Kaligrafi",
//     "MPK Seni Karawitan dan Tari Bali",
//     "MPK Seni Karawitan Jawa",
//     "MPK Seni Komik",
//     "MPK Seni Lukis",
//     "MPK Seni Teater",
//     "MPK Seni Wayang",
// ]

const RuleMatkulPeminatanIlkom = {}

const RuleMatkulPeminatanSI = {}

export const RuleList = [RuleMatkulUmum, RuleMatkulJurusanIlkom, RuleMatkulJurusanSI, RuleMatkulPeminatanIlkom, RuleMatkulPeminatanSI]

export const RuleListIlkom = [RuleMatkulUmum, RuleMatkulJurusanIlkom, RuleMatkulPeminatanIlkom]

export const RuleListSI = [RuleMatkulUmum, RuleMatkulJurusanSI, RuleMatkulPeminatanSI]

const ProcessRule = (key, value) => {
    if (value.length === 0) {
        return `bisa_ambil('${key}').`.toString();
    }
    return `bisa_ambil('${key}') :- ${value.map(rule => {
        return `sudah_ambil('${rule}')`.toString();
    }).join(",")
    }.`.toString()
}

const GetRule = (jurusan) => {
    if(jurusan === "Ilmu Komputer (Computer Science)"){
        return RuleListIlkom.map(obj => {
            return Object.entries(obj).map(([key, value]) => {
                return ProcessRule(key, value)
            }).join("")
        }).join("")
    }
    return RuleListSI.map(obj => {
        return Object.entries(obj).map(([key, value]) => {
            return ProcessRule(key, value)
        }).join("")
    }).join("")
}

const CourseRule = (jurusanUser) => {
    return `${GetRule(jurusanUser)}generate_matkul(X) :- bisa_ambil(X),\\+ sudah_ambil(X).`;
}

export default CourseRule;
