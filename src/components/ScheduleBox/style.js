import styled from "styled-components";

export const Container = styled.div`
    width: 100vw;
    padding: 20px;
    display: flex;
    align-items: center;
    flex-direction: column;
`

export const ScheduleContainer = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: baseline;
    padding: 20px;
`