export const SET_TOKEN = "session/SET_TOKEN";
export const CLEAR_TOKEN = "session/CLEAR_TOKEN";
export const SET_ACCOUNT = "session/SET_ACCOUNT";
export const CLEAR_ACCOUNT = "session/CLEAR_ACCOUNT";
export const LOADING_ACCOUNT = "session/LOADING_ACCOUNT";

export const sessionActions = {
  setToken: token => ({
    type: SET_TOKEN,
    payload: token
  }),

  clearToken: () => ({
    type: CLEAR_TOKEN
  }),

  setAccount: account => ({
    type: SET_ACCOUNT,
    payload: account
  }),
  clearAccount: () => ({
    type: CLEAR_ACCOUNT
  })
};

export function loadingAccount() {
  return {
    type: LOADING_ACCOUNT
  };
}
