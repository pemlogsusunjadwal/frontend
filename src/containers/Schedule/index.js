import React from 'react';
import { connect } from 'react-redux';

import { ScheduleContainer } from './style';
import schedule from "../../modules/schedule/thunk"
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import {
  CreateQuery,
  FormatList, GetAnswer,
  GetAnswersAsync,
  MakeProgramMatkulSuggestion,
  MakeProgramGenerateSchedule,
} from "../../ai_engine/prolog";
import ScheduleBox from '../../components/ScheduleBox';
import Button from '../../components/Button';

class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sks: 1,
      resultCount: 0,
      allResult: [],
      resultShow: [],
      scheduleResult: [],
      resultNow:0,
      doneComputation: null,
      doneScheduleComputation:null,
      found:null,
      disabled: false,
      scheduleResultCount: 0,
      schedulePicked: [],
      scheduleFound: null,
      scheduleSuggestionFound:null,
    }
  }

  componentDidMount = () => {
    const { listMatkul } = this.props.location.state;
    const { course, prosesData } = this.props;
    prosesData(listMatkul, course)
  }

  changeSks = e => {
    this.setState({
      sks: e.target.value
    })
  }

  clearResult = () => {
    this.setState({
      resultCount: 0,
      allResult: [],
      resultShow: [],
      resultNow:0,
      doneComputation: null,
      found:null,
      disabled:false
    })
    this.clearSchedule()
  }

  getSuggestion = () => {
        const sks = parseInt(this.state.sks);
        const { matkulExists,rawCourse } = this.props
        MakeProgramMatkulSuggestion(matkulExists,rawCourse);
        
        CreateQuery(`getMatkulCombination(X,${sks}).`);
        this.setState({
          disabled:true
        })
        GetAnswer(obj => {
          if(!obj){
            this.setState({
              resultCount: 0,
              allResult: [],
              resultShow: [],
              resultNow:0,
              found:false,
              scheduleResult: [],
              scheduleResultCount: 0,
              doneComputation: false,
              schedulePicked: [],
              scheduleFound: false
            })
          }else{
            const list = FormatList(obj.links.X);
            this.setState({
              resultCount: 1,
              allResult: [list],
              resultShow: list,
              resultNow:0,
              found:true,
              scheduleResult: [],
              scheduleResultCount: 0,
              doneComputation: false,
              schedulePicked: [],
              scheduleFound: false
            })
          }
        })
          GetAnswersAsync(obj => {
            const list = FormatList(obj.links.X);
            this.setState({
              resultCount: this.state.resultCount + 1,
              allResult: [...this.state.allResult,list]
            })
          },() => {
            this.setState({
              doneComputation: true,
            })
          });
    }

  changeResult = (val) => {
    const indexNow = this.state.resultNow + val;
    this.setState({
      resultNow:indexNow,
      resultShow:this.state.allResult[indexNow]
    })
  }

  clearSchedule = () => {
    this.setState({
      scheduleResult: [],
      scheduleResultCount: 0,
      doneScheduleComputation: null,
      schedulePicked: [],
      scheduleSuggestionFound: null
    })
  }

  generateSchedule = () => {
    const { rawCourse } = this.props;
    const { resultShow } = this.state;

    MakeProgramGenerateSchedule(rawCourse);
    CreateQuery(`generateJadwal(${JSON.stringify(resultShow).replace(/"/g, "'")}, X).`)

    GetAnswer(obj => {
      if(obj) {
        const list = FormatList(obj.links.X);
        this.setState({
          scheduleResult: [list],
          scheduleResultCount: 1,
          doneScheduleComputation: false,
          schedulePicked: list,
          scheduleSuggestionFound: true
        })
      } else {
        this.setState({
          scheduleResult: [],
          scheduleResultCount: 0,
          doneScheduleComputation: true,
          schedulePicked: [],
          scheduleSuggestionFound: false
        })
      }
    })
    GetAnswersAsync(obj => {
      if (obj) {
        const list = FormatList(obj.links.X);
          this.setState({
            scheduleResult: [...this.state.scheduleResult, list],
            scheduleResultCount: this.state.scheduleResultCount + 1,
          })
      }
    }, () => {
      this.setState({
        doneScheduleComputation: true,
      })
    })
  }

  render() {
    const { scheduleSuggestionFound } = this.state;
    return (
      <ScheduleContainer>
        <h1 align={"center"}>Schedule</h1>
        <Container fluid>
          <Row className="justify-content-md-center">
            <Col xl={5}>
              <h2>List matkul yang bisa diambil di jadwal</h2>
              {this.props.matkulExists.map(
                  (obj,idx) => <h3>{idx+1}. {obj}</h3>
              )}
              <h3>Sks yang diinginkan :</h3>
              <input disabled={this.state.disabled} id={"sks"} type="number" value={this.state.sks} max={24} min={1} onChange={this.changeSks}/>
              <button type="button" disabled={this.state.disabled} onClick={this.getSuggestion}>Cari</button>
              <button type="button" disabled={!this.state.disabled} onClick={this.clearResult}>Reset Hasil</button>
            </Col>
            <Col xl={5}>
              <h3>Matkul yang bisa dipilih</h3>
              {this.state.doneComputation !== null &&
                <div>
                  <h3>Result Count : {this.state.resultCount}</h3>
                  {this.state.doneComputation && <h5>Searching Done</h5>}
                  {this.state.found && <div>
                    <h4>Showing Result {this.state.resultNow + 1}</h4>
                    <Row>
                      <button type="button" disabled={this.state.resultNow === 0} onClick={() => this.changeResult(-1)}>Prev</button>
                      <button type="button" disabled={this.state.resultNow === this.state.allResult.length-1} onClick={() => this.changeResult(1)}>Next</button>
                      <Button disabled={this.state.scheduleSuggestionFound} onClick={this.generateSchedule} value="Generate"/>
                      <Button disabled={!this.state.scheduleSuggestionFound} onClick={this.clearSchedule} value="Clear Schedule"/>
                    </Row>
                    {this.state.resultShow.map((obj,idx) => <h3>{idx+1}. {obj}</h3>)}
                  </div>}
                  {!this.state.found && <h4>Not Found</h4>}
                </div>
              }
            </Col>
          </Row>
          <Row>
            {
              scheduleSuggestionFound && <ScheduleBox
                                  scheduleGenerated={this.state.scheduleResult} 
                                  resultCount={this.state.scheduleResultCount} 
                                  done={this.state.doneScheduleComputation}
                                />
            }
            { scheduleSuggestionFound !== null && !scheduleSuggestionFound && <h3 align="center">Schedule suggestion not found</h3>}

          </Row>
        </Container>
      </ScheduleContainer>
    );
  }
}

Schedule.propTypes = {

};

function mapStateToProps(state) {

  return {
    course: state.schedule.siakData.courses,
    rawCourse: state.schedule.nextRawCourses,
    matkulExists: state.schedule.matkulExists
  };
}

function mapDispatchToProps(dispatch) {
  return {
    prosesData: (listMatkul, course) => {
      dispatch(schedule.generateScheduleCombinationThunk(listMatkul, course))
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
