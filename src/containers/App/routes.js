import Landing from "../Landing";
import Schedule from "../Schedule";

export const routes = [
    {
        component:Landing,
        path:"/",
        exact:true
    },
    {
        component: Schedule,
        path:"/schedule",
        exact: true
    }
];
