import React from "react";
import { connect } from "react-redux";
import { LoginButtonContainer } from "./style";

import auth from "../../modules/auth/thunk";
import Button from "../Button";

class LoginButton extends React.Component {
  onClick = () => {
    const { login } = this.props;
    login();
  };

  render() {
    return (
      <LoginButtonContainer>
        <Button onClick={this.onClick} value="Login" />
      </LoginButtonContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: () => {
      dispatch(auth.loginThunk());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginButton);
