import auth from "./auth";
import schedule from "./schedule";

const spideyApi = {
  auth,
  schedule
};

export default spideyApi;
